Al momento de iniciar el proyecto decidi tener un cronjob que ejecute cada cierto tiempo y que sea el encargado de buscar el archivo con la informacion así como almacenar y verificar la data de los clientes. Elegi mysql ya que lo tenia configurado por otro proyectos. 

Elegi ROR como framework por lo simple que es crear el proyecto y por lo facil que se hace desarrollar en RUBY.

Posibles mejoras:

    - Hacer una copia en disco del archivo por si por algun motivo no logra leerse completo. Esto permitiria hacer solo una llamada a la api por cada intervalo de tiempo y al finalizar la lectura se elimina.

# Endpoints

    /clients
        return array with clients
        Example: 
        [{"id":1,"id_api":"d7decb93944648c7b61358b442d27565","email":"codi_steuber@mante.name","first_name":"Salvador","last_name":"Torp","job":"Administration Supervisor","country":"Turks and Caicos Islands","address":"30110 Waltraud Mews","zip_code":"43001","phone":"874.374.7123","created_at":"2019-12-29T19:03:05.000Z","updated_at":"2019-12-29T19:03:05.000Z"},{"id":2,"id_api":"3bc47750748b40d689f6fd6dd411834c","email":"francis@bayer.name","first_name":"Adrian","last_name":"Kunde","job":"Lead Strategist","country":"Saudi Arabia","address":"6224 Thiel Pass","zip_code":"46442","phone":"(677) 946-9741","created_at":"2019-12-29T19:15:07.000Z","updated_at":"2019-12-29T19:15:07.000Z"}]

    /clients/:id
        return a client
        Example: 
        {"id":1,"id_api":"d7decb93944648c7b61358b442d27565","email":"codi_steuber@mante.name","first_name":"Salvador","last_name":"Torp","job":"Administration Supervisor","country":"Turks and Caicos Islands","address":"30110 Waltraud Mews","zip_code":"43001","phone":"874.374.7123","created_at":"2019-12-29T19:03:05.000Z","updated_at":"2019-12-29T19:03:05.000Z"}

    /clients_api/:id_api
        return a client (using as id search the id provided by the api)
        Example: 
        {"id":1,"id_api":"d7decb93944648c7b61358b442d27565","email":"codi_steuber@mante.name","first_name":"Salvador","last_name":"Torp","job":"Administration Supervisor","country":"Turks and Caicos Islands","address":"30110 Waltraud Mews","zip_code":"43001","phone":"874.374.7123","created_at":"2019-12-29T19:03:05.000Z","updated_at":"2019-12-29T19:03:05.000Z"}

    /clients/:id/buys
        return a array with buys by client
        Example: 
        [{"id":1,"id_api":"fef76f08323f48798f09d006dc522c6b","currency":"ARS","total":103068389,"total_discounts":1628128,"total_with_discounts":101440261,"date":"20200116","client_id":2,"created_at":"2019-12-29T19:18:57.000Z","updated_at":"2019-12-29T19:18:57.000Z"},{"id":10,"id_api":"fef76f08323f48798f09d006dc522c6b","currency":"ARS","total":103068389,"total_discounts":1628128,"total_with_discounts":101440261,"date":"20200116","client_id":2,"created_at":"2019-12-29T19:24:04.000Z","updated_at":"2019-12-29T19:24:04.000Z"}]

    /clients/:id/balances?currency=XXX [options ARS y USD]
        return the balance by client
        Example: 
        {"client_id":2,"currency":"ARS","paid_out":0,"pending":202880522}


