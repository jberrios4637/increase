# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version
    2.6.x

* Configuration
    Run bundle install
    Configurate file database.yml (mysql or sqlite)

* Database creation
    Run rake db:create

* Database initialization
    Run rails db:migrate

* Crontab configuration
    - Prod
        whenever --update-crontab
    - Dev
        whenever --update-crontab --set environment=development

* Verify cronjob
    run crontab -l



