class CreateBuys < ActiveRecord::Migration[5.2]
  def change
    create_table :buys do |t|
      t.string :id_api
      t.integer :currency
      t.bigint :total
      t.bigint :total_discounts
      t.bigint :total_with_discounts
      t.string :date
      t.references :client, foreign_key: true

      t.timestamps
    end
  end
end
