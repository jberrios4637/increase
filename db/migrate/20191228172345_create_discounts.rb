class CreateDiscounts < ActiveRecord::Migration[5.2]
  def change
    create_table :discounts do |t|
      t.string :id_api
      t.bigint :amount
      t.integer :kind
      t.references :buy, foreign_key: true

      t.timestamps
    end
  end
end
