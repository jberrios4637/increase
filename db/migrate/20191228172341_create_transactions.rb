class CreateTransactions < ActiveRecord::Migration[5.2]
  def change
    create_table :transactions do |t|
      t.string :id_api
      t.bigint :amount
      t.integer :kind
      t.references :buy, foreign_key: true

      t.timestamps
    end
  end
end
