class ClientsController < ApplicationController
  before_action :find, only: [:show]

  def index
    @clients = Client.all()
    render json: @clients, status: :ok
  end

  def show 
    render json: @client, status: :ok
  end

  def show_by_api
    @client = Client.find_by({id_api: params[:id]})
    render json: @client, status: :ok
  end

  private
    def find
      @client = Client.find(params[:id])
    end
end
