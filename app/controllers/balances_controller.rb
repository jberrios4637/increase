class BalancesController < ApplicationController
  def show 
    @client = Client.find(params[:id])
    paid_out = 0
    pending = 0
    
    t = Time.now
    current_date = t.strftime("%Y/%m/%d")
    @client.buys.each do |buy|
      if buy.currency == params[:currency]
        if current_date >= DateTime.parse(buy.date, "%Y/%m/%d")
          paid_out = paid_out + buy.total_with_discounts
        else
          pending = pending + buy.total_with_discounts
        end
      end
    end

    render :json => {client_id: @client.id, currency: params[:currency], paid_out: paid_out, pending: pending}, status: :ok
  end
end
