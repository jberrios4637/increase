class BuysController < ApplicationController
  def index
    @client = Client.find(params[:id])
    @client.buys.each do |buy|
      buy.discounts
      buy.transactions
    end
    render json: @client.buys, status: :ok
  end
end
