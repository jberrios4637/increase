class Discount < ApplicationRecord
  belongs_to :buy
  validates :id_api, :amount, :kind, presence: true
end
