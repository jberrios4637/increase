class Buy < ApplicationRecord
  belongs_to :client
  has_many :transactions
  has_many :discounts
  validates :id_api, :currency, :total, :total_discounts, :total_with_discounts, presence: true
  enum currency: [:ARS , :USD]

end
