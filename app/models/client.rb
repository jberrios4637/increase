class Client < ApplicationRecord
    validates :id_api, :email, :first_name, :last_name, :job, :country, :address, :zip_code, :phone, presence: true
    has_many :buys
end
