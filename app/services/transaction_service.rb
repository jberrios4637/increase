require 'rest-client'
require 'json'

class TransactionService
  
  def get_data
    response = RestClient.get 'https://increase-transactions.herokuapp.com/file.txt', {:Authorization => 'Bearer 1234567890qwertyuiopasdfghjklzxcvbnm'}
  end

  def process_data data
    transactions_tmp = []
    discounts_tmp    = []
    buy_tmp = {}
    file = data.to_str.split("\n")
    file.each do |line|
    case line.byteslice(0)
      when "1"
        if line.length == 78
          buy_tmp = {id_api: line.byteslice(1..32), currency: line.byteslice(36..38).to_i, total: line.byteslice(39..51), total_discounts: line.byteslice(52..64), total_with_discounts: line.byteslice(65..77)}
        end
      when "2"
        if line.length == 52
          transactions_tmp.push({id_api: line.byteslice(1..32) , amount: line.byteslice(34..46) , kind: line.byteslice(51)})
        end
      when "3"
        if line.length == 50
          discounts_tmp.push({id_api: line.byteslice(1..32) , amount: line.byteslice(34..46) , kind: line.byteslice(49)})
        end
      when "4"
        if line.length == 56
          buy_tmp["date"] = line.byteslice(16..23);

          @client = Client.find_by({id_api: line.byteslice(24..56)})
          if !@client 
            @client = self.get_client_api(line.byteslice(24..56))
            if @client
              @client.save
            end
          end

          if @client
            if Buy.exists?({id_api: buy_tmp["id_api"]})
              @buy = Buy.find_by({id_api: buy_tmp["id_api"]})
            else
              @buy = Buy.new(buy_tmp)
            end

            @client.buys << @buy
            transactions_tmp.each do |transaction|
              if !Transaction.exists?({id_api: transaction["id_api"]})
                @t = Transaction.new(transaction)
                @buy.transactions << @t
              end
            end

            discounts_tmp.each do |discount|
              if !Discount.exists?({id_api: discount["id_api"]})
                @d = Discount.new(discount)
                @buy.discounts << @d
              end
            end
          end
        end

        transactions_tmp = []
        discounts_tmp    = []
        buy_tmp = {}
      end
    end
  end

  def get_client_api id
    response = RestClient.get "https://increase-transactions.herokuapp.com/clients/#{id}", {:Authorization => 'Bearer 1234567890qwertyuiopasdfghjklzxcvbnm'}
    result = JSON.parse(response.to_str)
    if result != nil
      result.delete("id")
      result["id_api"] = id
      return Client.new(result)
    end

    return false
  end

  def run 
    file = self.get_data
    self.process_data(file)
  end
   
end