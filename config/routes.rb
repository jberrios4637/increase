Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get '/clients', to: 'clients#index'
  get '/clients/:id', to: 'clients#show'
  get '/clients_api/:id', to: 'clients#show_by_api'
  get '/clients/:id/buys', to: 'buys#index'
  get '/clients/:id/balances', to: 'balances#show'

  # Run test proccess load_data
  get '/test', to: 'test#run'
end
